<?php

namespace App\Http\Controllers;

class BaseController extends Controller
{
    const STATUS_OK = 200;
    const STATUS_NOT_FIND = 404;
    const STATUS_BAD_REQUEST = 400;

    /**
     * @param $data
     * @param array $meta
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    public function respond($data = [], $meta = [], $statusCode = 200)
    {
        return response()->json(['data' => $data, 'meta' => $meta], $statusCode);
    }

    /**
     * @param $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function badRequest($message, $data = null)
    {
        $index = 0;
        foreach ($message as $key => $value) {
            $myError['invalid_params'][$index]['field'] = $key;
            $myError['invalid_params'][$index]['message'] = $value;
            $index++;
        }

        return response()->json(['data' => $data ? $data : [], 'meta' => [], 'error' => $myError], self::STATUS_BAD_REQUEST);
    }


    /**
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    public function success()
    {
        return response()->json([], self::STATUS_OK);

    }

    /**
     * @param $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function validateResponse($message)
    {
        $index = 0;
        foreach ($message as $key => $value) {
            $myError['invalid_params'][$index]['field'] = $key;
            $myError['invalid_params'][$index]['message'] = $value[0];
            $index++;
        }
        return response()->json(['data' => [], 'meta' => [], 'error' => $myError], self::STATUS_BAD_REQUEST);

    }




    /**
     * @param $myError
     * @return \Illuminate\Http\JsonResponse
     */
    public function errorResponse($myError)
    {
        return response()->json(['error' => $myError], 500);
    }

}
