<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Post\PostController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::prefix('/post/{user}')->group(function (){
    Route::post('/',[PostController::class,'create']);
    Route::post('/{post}',[PostController::class,'update']);
    Route::delete('/{post}',[PostController::class,'delete']);
    Route::get('/{post}',[PostController::class,'view']);
    Route::get('/',[PostController::class,'list']);
});

