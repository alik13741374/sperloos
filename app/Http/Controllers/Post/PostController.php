<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\BaseController;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;


class PostController extends BaseController
{
    use HasMediaTrait;
    public function create(Request $request,User $user)
    {
        $validator = Validator::make($request->all(),[
            'title'=>'required|string|max:500',
            'content'=>'required|string|max:2000',
            'image'=>'required|image|mimes:jpeg,png,jpg,svg|max:2048',
        ]);
        if($validator->fails()){
            return $this->validateResponse($validator->errors()->toArray());
        }
        if($user->can('create',Post::class))
        {
            $post = new Post();
            $post->title = $request->get('title');
            $post->content = $request->get('content');
            $post->user_id = $user->id;
            $post->save();
            $post->addMediaFromRequest('image')->toMediaCollection('images');
            return $this->success();
        }
        else
             return $this->badRequest(['message' => 'permission denied']);
    }

    public function update(Request $request ,User $user,Post $post)
    {
        $validator = Validator::make($request->all(),[
            'title'=>'required|string|max:500',
            'content'=>'required|string|max:2000',
            'image'=>'required|image|mimes:jpeg,png,jpg,svg|max:2048',
        ]);
        if($validator->fails()){
            return $this->validateResponse($validator->errors()->toArray());
        }
        if($user->can('update',$post)) {
            $post->update($request->all('title', 'content'));
            return $this->success();
        }
        else
            return $this->badRequest(['message' => 'permission denied']);
    }

    public function list(User $user)
    {
        $data = Post::get();
        foreach ($data as $D)
        {
            $D->thumb_nail = asset($D->getFirstMediaUrl('images','thumb'));
            $D->image = asset($D->getFirstMediaUrl('images'));
        }
        return $this->respond($data);
    }

    public function view( User $user,Post $post)
    {
        $post->thumb_nail = asset($post->getFirstMediaUrl('images','thumb'));
        $post->image = asset($post->getFirstMediaUrl('images'));
        return $this->respond($post);

    }

    public function delete(Request $request ,User $user,Post $post)
    {
        if($user->can('delete',$post)) {
            $post->delete();
            return $this->success();
        }
        else
            return $this->badRequest(['message' => 'permission denied']);
    }
}
