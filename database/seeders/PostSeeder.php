<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;



class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $posts=\App\Models\Post::factory(10)->create();

        $faker = Faker::create();
        foreach($posts as $post){
            $image = $faker->imageUrl(100,100, null, false);
            $post->addMediaFromUrl($image)->toMediaCollection('images');
        }
    }
}
